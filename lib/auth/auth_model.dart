import 'package:equatable/equatable.dart';

class Auth extends Equatable {
  final String hostname;
  final String username;
  final String password;

  Auth({this.hostname, this.username, this.password});

  factory Auth.fromJson(Map<String, dynamic> json) => Auth(
      hostname: json['hostname'] as String,
      username: json['username'] as String,
      password: json['password'] as String);

  Map<String, dynamic> toJson() => <String, dynamic>{
        'hostname': this.hostname,
        'username': this.username,
        'password': this.password
      };

  @override
  List<Object> get props => [hostname, username, password];

  @override
  bool get stringify => true;
}
