import 'package:equatable/equatable.dart';

import 'auth_model.dart';

abstract class AuthState extends Equatable {
  final Auth auth;
  AuthState(this.auth);
}

class AuthInitial extends AuthState {
  AuthInitial() : super(Auth(hostname: '', username: '', password: ''));

  @override
  List<Object> get props => [auth];
}

class AuthTesting extends AuthState {
  AuthTesting(Auth auth) : super(auth);

  @override
  List<Object> get props => [auth];
}

class AuthComplete extends AuthState {
  AuthComplete(Auth auth) : super(auth);

  @override
  List<Object> get props => [auth];
}

class AuthError extends AuthState {
  final String message;
  AuthError(Auth auth, this.message) : super(auth);

  @override
  List<Object> get props => [auth];
}
