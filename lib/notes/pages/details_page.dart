import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_markdown/flutter_markdown.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import '../../auth/auth_model.dart';
import '../models/note.dart';
import '../models/extended_note.dart';
import '../models/checklist.dart';
import '../models/document.dart';
import '../models/link.dart';
import '../note_repository.dart';
import '../notes_cubit.dart';
import '../notes_state.dart';

class DetailsPage extends StatefulWidget {
  final Auth auth;
  final Note note;
  const DetailsPage({Key key, @required this.auth, @required this.note})
      : super(key: key);

  @override
  _DetailsPageState createState() => _DetailsPageState();
}

class _DetailsPageState extends State<DetailsPage> {
  String _editing;
  ExtendedNote _editedExtendedNote;

  Future<bool> _save(BuildContext context) async {
    if (widget.note.filename.isEmpty) {
      if (_editedExtendedNote.isEmpty())
        Navigator.pop(context);
      else
        Navigator.pop(context,
            {'add': widget.note.copyWith(extendedNote: _editedExtendedNote)});
      return false;
    } else if (_editedExtendedNote != widget.note.extendedNote) {
      if (_editedExtendedNote.isEmpty())
        Navigator.pop(context, {'delete': widget.note});
      else
        Navigator.pop(context, {
          'edit':
              widget.note.copyWith(etag: '', extendedNote: _editedExtendedNote)
        });
      return false;
    } else
      return true;
  }

  _delete(BuildContext context) async {
    if (widget.note.filename.isEmpty)
      Navigator.pop(context);
    else
      Navigator.pop(context, {'delete': widget.note});
  }

  @override
  void initState() {
    super.initState();
    _editedExtendedNote = widget.note.extendedNote.copyWith();
  }

  @override
  Widget build(BuildContext context) => BlocProvider<NotesCubit>(
      create: (context) => NotesCubit(NoteRepository(widget.auth)),
      child: BlocBuilder<NotesCubit, NotesState>(
          builder: (context, state) => WillPopScope(
              onWillPop: () => _save(context),
              child: Scaffold(
                  appBar: AppBar(title: Text('NextNotes'), actions: [
                    IconButton(
                        icon: Icon(Icons.delete),
                        onPressed: () => _delete(context))
                  ]),
                  body: GestureDetector(
                      onTap: () => setState(() => _editing = null),
                      child: Container(
                          padding: EdgeInsets.all(15),
                          constraints: BoxConstraints.expand(),
                          child: Builder(builder: (BuildContext context) {
                            switch (_editedExtendedNote.runtimeType) {
                              case Link:
                                final Link link = _editedExtendedNote as Link;
                                return ListView(children: [
                                  Image.memory(link.icon),
                                  (_editing == 'title')
                                      ? TextField(
                                          controller: TextEditingController(
                                              text: link.title),
                                          style: TextStyle(
                                              fontSize: 20,
                                              fontWeight: FontWeight.bold),
                                          onChanged: (value) =>
                                              _editedExtendedNote =
                                                  (_editedExtendedNote as Link)
                                                      .copyWith(title: value))
                                      : InkWell(
                                          onTap: () => setState(
                                              () => _editing = 'title'),
                                          child: Text(link.title,
                                              style: TextStyle(
                                                  fontSize: 20,
                                                  fontWeight:
                                                      FontWeight.bold))),
                                  (_editing == 'url')
                                      ? TextField(
                                          controller: TextEditingController(
                                              text: link.url),
                                          onChanged: (value) =>
                                              _editedExtendedNote =
                                                  (_editedExtendedNote as Link)
                                                      .copyWith(url: value))
                                      : InkWell(
                                          onTap: () =>
                                              setState(() => _editing = 'url'),
                                          child: Text(link.url))
                                ]);
                              case Checklist:
                                final Checklist checklist =
                                    _editedExtendedNote as Checklist;
                                return ListView.builder(
                                    physics: NeverScrollableScrollPhysics(),
                                    shrinkWrap: true,
                                    itemCount: checklist.checklist.length + 1,
                                    itemBuilder: (BuildContext context,
                                            int index) =>
                                        (index == checklist.checklist.length)
                                            ? ListTile(
                                                onTap: () => setState(() {
                                                      checklist.checklist[''] =
                                                          false;
                                                      _editing =
                                                          'checklist[$index]';
                                                    }),
                                                leading: Icon(Icons.add),
                                                title: Text(
                                                    AppLocalizations.of(context)
                                                        .listItem))
                                            : ListTile(
                                                leading: IconButton(
                                                    onPressed: () => setState(
                                                        () => (_editedExtendedNote
                                                                as Checklist)
                                                            .checklist
                                                            .update(
                                                                checklist
                                                                    .checklist
                                                                    .keys
                                                                    .elementAt(
                                                                        index),
                                                                (value) =>
                                                                    !value)),
                                                    icon: Icon((checklist.checklist[
                                                            checklist
                                                                .checklist.keys
                                                                .elementAt(index)])
                                                        ? Icons.check_box
                                                        : Icons.check_box_outline_blank)),
                                                title: (_editing == 'checklist[$index]')
                                                    ? TextField(
                                                        controller: TextEditingController(
                                                            text: checklist
                                                                .checklist.keys
                                                                .elementAt(
                                                                    index)),
                                                        onChanged: (value) => _editedExtendedNote = (_editedExtendedNote as Checklist).copyWith(
                                                            checklist: Map.fromIterable(
                                                                checklist
                                                                    .checklist
                                                                    .entries,
                                                                key: (item) =>
                                                                    (item.key == checklist.checklist.keys.elementAt(index)) ? value : item.key,
                                                                value: (item) => item.value)))
                                                    : InkWell(onTap: () => setState(() => _editing = 'checklist[$index]'), child: Text(checklist.checklist.keys.elementAt(index))),
                                                trailing: InkWell(
                                                    onTap: () => setState(() =>
                                                        checklist.checklist
                                                            .remove(checklist
                                                                .checklist.keys
                                                                .elementAt(
                                                                    index))),
                                                    child: Icon(Icons.close)),
                                              ));
                              case Document:
                                final Document document =
                                    _editedExtendedNote as Document;
                                return (_editing == 'text')
                                    ? TextField(
                                        controller: TextEditingController(
                                            text: document.text),
                                        maxLines: null,
                                        onChanged: (value) =>
                                            _editedExtendedNote =
                                                (_editedExtendedNote
                                                        as Document)
                                                    .copyWith(text: value))
                                    : InkWell(
                                        onTap: () =>
                                            setState(() => _editing = 'text'),
                                        child: Markdown(data: document.text));
                              default:
                                return null;
                            }
                          })))))));
}
