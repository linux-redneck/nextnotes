import 'dart:async';
import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_markdown/flutter_markdown.dart';
import 'package:flutter_speed_dial/flutter_speed_dial.dart';
import 'package:loading_overlay/loading_overlay.dart';
import 'package:receive_sharing_intent/receive_sharing_intent.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import '../../auth/auth_model.dart';
import '../models/note.dart';
import '../models/extended_note.dart';
import '../models/checklist.dart';
import '../models/document.dart';
import '../models/link.dart';
import '../note_repository.dart';
import '../notes_cubit.dart';
import '../notes_state.dart';
import 'details_page.dart';

class NotesPage extends StatefulWidget {
  final Auth auth;
  const NotesPage({Key key, @required this.auth}) : super(key: key);

  @override
  _NotesPageState createState() => _NotesPageState();
}

class _NotesPageState extends State<NotesPage> {
  StreamSubscription _intentDataStreamSubscription;
  NotesCubit _notesCubit;
  bool _loadingSharedNote = false;

  @override
  void initState() {
    super.initState();
    _notesCubit = NotesCubit(NoteRepository(widget.auth));
    if (_notesCubit.state is NotesInitial)
      _notesCubit.getNotes();
    else
      _notesCubit.updateNotes();
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    _intentDataStreamSubscription =
        ReceiveSharingIntent.getTextStream().listen((String value) async {
      if (value != null) {
        ExtendedNote extNote;
        if (value.startsWith('http')) {
          setState(() => _loadingSharedNote = true);
          extNote = await Link.fromURL(value);
          setState(() => _loadingSharedNote = false);
        } else
          extNote = Document(text: value);
        _gotoDetails(Note.fromExtendedNote(extNote));
      }
    }, onError: (err) {
      print("getLinkStream error: $err");
    });
    ReceiveSharingIntent.getInitialText().then((String value) async {
      if (value != null) {
        ExtendedNote extNote;
        if (value.startsWith('http')) {
          setState(() => _loadingSharedNote = true);
          extNote = await Link.fromURL(value);
          setState(() => _loadingSharedNote = false);
        } else
          extNote = Document(text: value);
        _gotoDetails(Note.fromExtendedNote(extNote));
      }
    });
  }

  @override
  void dispose() {
    _intentDataStreamSubscription.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) => BlocProvider<NotesCubit>(
      create: (context) => _notesCubit,
      child: BlocConsumer<NotesCubit, NotesState>(
          listener: (context, state) {
            if (state is NotesError)
              ScaffoldMessenger.of(context).showSnackBar(
                SnackBar(
                    content: Text(state.message, textAlign: TextAlign.center)),
              );
          },
          builder: (context, state) => Scaffold(
              appBar: AppBar(title: Text('NextNotes'), actions: [
                if (state is NotesLoading)
                  Padding(
                      padding: EdgeInsets.only(right: 20),
                      child: Center(
                          child: CircularProgressIndicator(
                              valueColor: AlwaysStoppedAnimation<Color>(
                                  Colors.orange[300]))))
              ]),
              body: LoadingOverlay(
                  isLoading: _loadingSharedNote,
                  child: ListView.builder(
                      itemCount: state.notes.length,
                      itemBuilder: (BuildContext context, int index) {
                        final note = state.notes[index];
                        switch (note.extendedNote.runtimeType) {
                          case Link:
                            final Link link = note.extendedNote as Link;
                            return Card(
                                child: ListTile(
                                    title: Text(link.title),
                                    subtitle: Text(link.url),
                                    leading: Image.memory(link.icon),
                                    trailing: IconButton(
                                        icon: Icon(Icons.launch),
                                        onPressed: () async =>
                                            await launch(link.url)),
                                    onTap: () => _gotoDetails(note)));
                            break;
                          case Checklist:
                            final Checklist checklist =
                                note.extendedNote as Checklist;
                            final List<String> items = [];
                            int checked = 0;
                            checklist.checklist.forEach((key, value) {
                              if (!value)
                                items.add(key);
                              else
                                checked++;
                            });
                            List<Widget> extraLines = [];
                            if (items.length > 3)
                              extraLines.add(ListTile(
                                  visualDensity: VisualDensity(
                                      horizontal: 0,
                                      vertical: VisualDensity.minimumDensity),
                                  leading: Icon(Icons.check_box_outline_blank),
                                  title: Text(
                                      '+ ${items.length - 3} ${AppLocalizations.of(context).uncheckedItems}')));
                            if (checked > 0)
                              extraLines.add(ListTile(
                                  visualDensity: VisualDensity(
                                      horizontal: 0, vertical: -4),
                                  leading: Icon(Icons.check_box),
                                  title: Text(
                                      '+ $checked ${AppLocalizations.of(context).checkedItems}')));
                            return Card(
                                child: InkWell(
                                    onTap: () => _gotoDetails(note),
                                    child: ListView.builder(
                                        physics: NeverScrollableScrollPhysics(),
                                        shrinkWrap: true,
                                        itemCount: items.take(3).length +
                                            extraLines.length,
                                        itemBuilder: (BuildContext context,
                                                int index) =>
                                            (index == items.take(3).length)
                                                ? extraLines[0]
                                                : (index ==
                                                        items.take(3).length +
                                                            1)
                                                    ? extraLines[1]
                                                    : ListTile(
                                                        visualDensity:
                                                            VisualDensity(
                                                                horizontal: 0,
                                                                vertical: -4),
                                                        leading: Icon(Icons
                                                            .check_box_outline_blank),
                                                        title: Text(
                                                            items[index])))));
                          case Document:
                            final Document text = note.extendedNote as Document;
                            List<String> lines =
                                LineSplitter.split(text.text).toList();
                            return Card(
                                child: InkWell(
                                    onTap: () => _gotoDetails(note),
                                    child: Markdown(
                                        physics: NeverScrollableScrollPhysics(),
                                        data: lines.take(5).join('\n'),
                                        shrinkWrap: true)));
                          default:
                            return null;
                        }
                      })),
              floatingActionButton: SpeedDial(
                  icon: Icons.add,
                  activeIcon: Icons.close,
                  backgroundColor: Theme.of(context).primaryColor,
                  overlayOpacity: 0,
                  children: [
                    SpeedDialChild(
                        child: Icon(Icons.subject),
                        backgroundColor: Theme.of(context).primaryColor,
                        onTap: () => _gotoDetails(
                            Note.fromExtendedNote(Document(text: '')))),
                    SpeedDialChild(
                        child: Icon(Icons.check_box),
                        backgroundColor: Theme.of(context).primaryColor,
                        onTap: () => _gotoDetails(
                            Note.fromExtendedNote(Checklist(checklist: {})))),
                    SpeedDialChild(child: Icon(Icons.mic), onTap: () {}),
                    SpeedDialChild(child: Icon(Icons.image), onTap: () {}),
                    SpeedDialChild(child: Icon(Icons.insert_link), onTap: () {})
                  ]))));

  _gotoDetails(Note note) => Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) =>
                  DetailsPage(auth: widget.auth, note: note))).then((val) {
        if (val is Map<String, Note>) {
          if (val.containsKey('add')) {
            _notesCubit.addNote(val['add']);
          } else if (val.containsKey('edit')) {
            _notesCubit.editNote(val['edit']);
          } else if (val.containsKey('delete')) {
            _notesCubit.deleteNote(val['delete']);
          }
        }
      });
}
