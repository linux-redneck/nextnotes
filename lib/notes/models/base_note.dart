import 'dart:io';
import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';
import 'package:path/path.dart';
import 'package:xml/xml.dart';

import 'extended_note.dart';
import 'note.dart';

class BaseNote extends Equatable {
  final String filename;
  final int modified;
  final String etag;
  final String contenttype;

  BaseNote(
      {@required this.filename,
      @required this.modified,
      @required this.etag,
      @required this.contenttype});

  factory BaseNote.fromXml(XmlElement xml) {
    final prop = xml.getElement('d:propstat').getElement('d:prop');
    return BaseNote(
        filename: basename(xml.getElement('d:href').text),
        modified: HttpDate.parse(prop.getElement('d:getlastmodified').text)
            .millisecondsSinceEpoch,
        etag: prop.getElement('d:getetag').text,
        contenttype: prop.getElement('d:getcontenttype').text);
  }

  factory BaseNote.fromJson(Map<String, dynamic> json) => BaseNote(
      filename: json['filename'] as String,
      modified: json['modified'] as int,
      etag: json['etag'] as String,
      contenttype: json['contenttype'] as String);

  Map<String, dynamic> toJson() => <String, dynamic>{
        'filename': this.filename,
        'modified': this.modified,
        'etag': this.etag,
        'contenttype': this.contenttype
      };

  Note toNote({@required ExtendedNote extendedNote}) => Note(
      filename: this.filename,
      modified: this.modified,
      etag: this.etag,
      contenttype: this.contenttype,
      extendedNote: extendedNote);

  @override
  List<Object> get props => [filename, modified, etag, contenttype];

  @override
  bool get stringify => true;
}
