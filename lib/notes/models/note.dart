import 'package:flutter/foundation.dart';

import 'base_note.dart';
import 'extended_note.dart';

class Note extends BaseNote {
  final ExtendedNote extendedNote;

  Note(
      {@required String filename,
      @required int modified,
      @required String etag,
      @required String contenttype,
      @required this.extendedNote})
      : super(
            filename: filename,
            modified: modified,
            etag: etag,
            contenttype: contenttype);

  factory Note.fromJson(Map<String, dynamic> json) => Note(
      filename: json['filename'] as String,
      modified: json['modified'] as int,
      etag: json['etag'] as String,
      contenttype: json['contenttype'] as String,
      extendedNote: json['extendedNote'] == null
          ? null
          : ExtendedNote.fromJson(
              json['extendedNote'] as Map<String, dynamic>));

  factory Note.fromExtendedNote(ExtendedNote extendedNote) => Note(
      filename: '',
      modified: 0,
      etag: '',
      contenttype: '',
      extendedNote: extendedNote);

  Map<String, dynamic> toJson() => <String, dynamic>{
        'filename': this.filename,
        'modified': this.modified,
        'etag': this.etag,
        'contenttype': this.contenttype,
        'extendedNote': this.extendedNote.toJson()
      };

  Note copyWith(
          {String filename,
          String modified,
          String etag,
          String contenttype,
          ExtendedNote extendedNote}) =>
      Note(
          filename: filename ?? this.filename,
          modified: modified ?? this.modified,
          etag: etag ?? this.etag,
          contenttype: contenttype ?? this.contenttype,
          extendedNote: extendedNote ?? this.extendedNote);

  BaseNote toBaseNote() => BaseNote(
      filename: this.filename,
      modified: this.modified,
      etag: this.etag,
      contenttype: this.contenttype);

  @override
  List<Object> get props =>
      [filename, modified, etag, contenttype, extendedNote];
}
