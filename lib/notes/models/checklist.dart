import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';

import 'extended_note.dart';

class Checklist extends Equatable implements ExtendedNote {
  final Map<String, bool> checklist;

  Checklist({@required this.checklist});

  factory Checklist.fromJson(Map<String, dynamic> json) => Checklist(
      checklist: (json['checklist'] as Map<String, dynamic>)
          ?.map((k, e) => MapEntry(k, e as bool)));

  Map<String, dynamic> toJson() =>
      <String, dynamic>{'type': 'checklist', 'checklist': this.checklist};

  Checklist copyWith({Map<String, bool> checklist}) =>
      Checklist(checklist: checklist ?? Map.of(this.checklist));

  bool isEmpty() => this.checklist.isEmpty;

  @override
  List<Object> get props => [checklist];
}
