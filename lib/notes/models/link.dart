import 'dart:math';
import 'dart:typed_data';

import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';
import 'package:html/dom.dart';
import 'package:http/http.dart' as http;
import 'package:html/parser.dart';
import 'package:image/image.dart' as imglib;

import 'extended_note.dart';

class Link extends Equatable implements ExtendedNote {
  final String url;
  final String iconFile;
  final Uint8List icon;
  final String title;

  Link(
      {@required this.url,
      @required this.iconFile,
      @required this.icon,
      @required this.title});

  factory Link.fromJson(Map<String, dynamic> json) => Link(
      url: json['url'] as String,
      iconFile: json['iconFile'] as String,
      icon: Uint8List.fromList(json['icon'].codeUnits),
      title: json['title'] as String);

  Map<String, dynamic> toJson() => <String, dynamic>{
        'type': 'link',
        'url': this.url,
        'iconFile': this.iconFile,
        'icon': String.fromCharCodes(this.icon),
        'title': this.title
      };

  Link copyWith({String url, String iconFile, Uint8List icon, String title}) =>
      Link(
          url: url ?? this.url,
          iconFile: iconFile ?? this.iconFile,
          icon: icon ?? this.icon,
          title: title ?? this.title);

  static Future<Link> fromURL(String url) async {
    final Uri uri = Uri.parse(url.replaceFirst('http://', 'https://'));
    final Document document = parse(await http.read(uri));
    final String title = document.getElementsByTagName('title').first.text;
    String iconFile;
    Element ogImage = document.getElementsByTagName('meta').firstWhere(
        (meta) => meta.attributes['property'] == 'og:image',
        orElse: () => null);
    if (ogImage != null)
      iconFile = ogImage.attributes['content'];
    else {
      List<Element> links = document.getElementsByTagName('link');
      Iterable<Element> icons =
          links.where((link) => link.attributes['rel'] == 'icon');
      Element icon;
      if (icons.isNotEmpty)
        icon = icons.reduce((value, element) =>
            (int.parse(value.attributes['sizes'].split('x')[0]) >
                    int.parse(element.attributes['sizes'].split('x')[0]))
                ? value
                : element);
      if (icon != null)
        iconFile = icon.attributes['href'];
      else {
        Element appleTouchIcon = links.firstWhere(
            (link) => link.attributes['rel'] == 'apple-touch-icon',
            orElse: () => null);
        if (appleTouchIcon != null)
          iconFile = appleTouchIcon.attributes['href'];
        else
          iconFile = '/favicon.ico';
      }
    }
    imglib.Image icon;
    if (iconFile != null) {
      if (!iconFile.startsWith('http'))
        iconFile = uri.resolve(iconFile).toString();
      try {
        final response = await http
            .get(Uri.parse(iconFile.replaceFirst('http://', 'https://')));
        icon = imglib.decodeImage(response.bodyBytes);
      } catch (e) {
        icon = imglib.Image(480, 480);
      }
    }
    if (icon == null) icon = imglib.Image(480, 480);
    if (icon.width > 480 || icon.height > 480 || icon.width != icon.height)
      icon = imglib.copyResizeCropSquare(
          icon, min(min(icon.width, icon.height), 480));
    return Link(
        url: uri.toString(),
        iconFile: iconFile,
        icon: imglib.encodeJpg(icon, quality: 75),
        title: title);
  }

  bool isEmpty() => this.url.isEmpty;

  @override
  List<Object> get props => [url, iconFile, icon, title];
}
