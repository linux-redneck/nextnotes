import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';

import 'extended_note.dart';

class Document extends Equatable implements ExtendedNote {
  final String text;

  Document({@required this.text});

  factory Document.fromJson(Map<String, dynamic> json) =>
      Document(text: json['text'] as String);

  Map<String, dynamic> toJson() =>
      <String, dynamic>{'type': 'document', 'text': this.text};

  Document copyWith({String text}) => Document(text: text ?? this.text);

  bool isEmpty() => this.text.isEmpty;

  @override
  List<Object> get props => [text];
}
