import 'dart:convert';
import 'dart:typed_data';
import 'dart:io';

import 'package:http/http.dart';
import 'package:http_auth/http_auth.dart';
import 'package:xml/xml.dart';

import '../auth/auth_model.dart';
import 'models/base_note.dart';
import 'models/checklist.dart';
import 'models/document.dart';
import 'models/link.dart';
import 'models/note.dart';

class NoteRepository {
  final Auth auth;
  final BasicAuthClient client;
  final Uri baseUrl;

  NoteRepository(this.auth)
      : client = BasicAuthClient(auth.username, auth.password),
        baseUrl = Uri(
            scheme: 'https',
            host: auth.hostname,
            path: 'cloud/remote.php/dav/files/${auth.username}/NextNotes/');

  Future<List<BaseNote>> getBaseNotes() async {
    final response = await client.send(Request('PROPFIND', baseUrl));
    if (response.statusCode == 404) {
      await client.send(Request('MKCOL', baseUrl));
      client.send(Request('MKCOL', baseUrl.resolve('MetaData')));
      return [];
    }
    final xmlDocument =
        XmlDocument.parse(await response.stream.bytesToString());
    final List<BaseNote> baseNotes = [];
    xmlDocument
        .findAllElements('d:response')
        .where((el) => !el.getElement('d:href').text.endsWith('/'))
        .forEach((el) => baseNotes.add(BaseNote.fromXml(el)));
    baseNotes.sort((a, b) => -a.modified.compareTo(b.modified));
    return baseNotes;
  }

  Future<Note> completeNote(BaseNote baseNote) async {
    var content = await client.read(baseUrl.resolve(baseNote.filename));
    switch (baseNote.contenttype) {
      case 'text/markdown':
        return baseNote.toNote(extendedNote: Document(text: content));
      case 'application/yaml':
        final lines = LineSplitter.split(content);
        final Map<String, bool> checklist = {};
        for (var i = 0; i < lines.length; i = i + 2)
          checklist[lines.elementAt(i).substring(2)] =
              (lines.elementAt(i + 1).substring(2) == 'true');
        return baseNote.toNote(extendedNote: Checklist(checklist: checklist));
      case 'application/internet-shortcut':
        final lines = LineSplitter.split(content);
        String url;
        String iconFile;
        Uint8List icon;
        String title;
        lines.forEach((line) {
          if (line.startsWith('URL='))
            url = line.substring(4);
          else if (line.startsWith('IconFile='))
            iconFile = line.substring(9);
          else if (line.startsWith('Title=')) title = line.substring(6);
        });
        if (iconFile.isNotEmpty) {
          var response =
              await client.get(baseUrl.resolve('MetaData/$iconFile'));
          icon = response.bodyBytes;
        }
        return baseNote.toNote(
            extendedNote:
                Link(url: url, iconFile: iconFile, icon: icon, title: title));
      default:
        return null;
    }
  }

  Future<void> editNote(Note note) async {
    String content;
    switch (note.extendedNote.runtimeType) {
      case Link:
        content = '[InternetShortcut]';
        final link = note.extendedNote as Link;
        content += '\nURL=${link.url}';
        content += '\nTitle=${link.title}';
        content += '\nIconFile=${link.iconFile}';
        break;
      case Document:
        final text = note.extendedNote as Document;
        content = text.text;
        break;
      case Checklist:
        final checklist = note.extendedNote as Checklist;
        content = '';
        checklist.checklist.forEach(
            (key, value) => content += '? $key\n: ${value.toString()}\n');
        break;
      default:
    }
    final response =
        await client.put(baseUrl.resolve(note.filename), body: content);
    if (response.statusCode != 204) throw (response.statusCode);
  }

  Future<void> deleteNote(Note note) async {
    if (note.extendedNote is Link)
      await client.delete(
          baseUrl.resolve('MetaData/${(note.extendedNote as Link).iconFile}'));
    final response = await client.delete(baseUrl.resolve(note.filename));
    if (response.statusCode != 204) throw (response.statusCode);
  }

  Future<Note> addNote(Note note) async {
    final DateTime dateTime = DateTime.now().toUtc();
    String filename =
        '${dateTime.year}${dateTime.month.toString().padLeft(2, '0')}${dateTime.day.toString().padLeft(2, '0')}T${dateTime.hour.toString().padLeft(2, '0')}${dateTime.minute.toString().padLeft(2, '0')}${dateTime.second.toString().padLeft(2, '0')}Z';
    String contenttype;
    String content;
    switch (note.extendedNote.runtimeType) {
      case Link:
        contenttype = 'application/internet-shortcut';
        content = '[InternetShortcut]';
        final link = note.extendedNote as Link;
        content += '\nURL=${link.url}';
        content += '\nTitle=${link.title}';
        client.put(baseUrl.resolve('MetaData/$filename.jpg'), body: link.icon);
        content += '\nIconFile=$filename.jpg';
        note = note.copyWith(
            extendedNote: (note.extendedNote as Link)
                .copyWith(iconFile: '$filename.jpg'));
        filename += '.url';
        break;
      case Document:
        filename += '.md';
        contenttype = 'text/markdown';
        final text = note.extendedNote as Document;
        content = text.text;
        break;
      case Checklist:
        filename += '.yml';
        contenttype = 'application/yaml';
        final checklist = note.extendedNote as Checklist;
        content = '';
        checklist.checklist.forEach(
            (key, value) => content += '? $key\n: ${value.toString()}\n');
        break;
      default:
    }
    final response = await client.put(baseUrl.resolve(filename), body: content);
    if (response.statusCode == 201)
      return Note(
          filename: filename,
          modified:
              HttpDate.parse(response.headers['date']).millisecondsSinceEpoch,
          etag: response.headers['etag'],
          contenttype: contenttype,
          extendedNote: note.extendedNote);
    else
      throw (response.statusCode);
  }
}
