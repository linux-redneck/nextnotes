import 'package:equatable/equatable.dart';

import 'models/note.dart';

abstract class NotesState extends Equatable {
  final List<Note> notes;
  NotesState(this.notes);
}

class NotesInitial extends NotesState {
  NotesInitial() : super([]);

  @override
  List<Object> get props => [notes];
}

class NotesLoading extends NotesState {
  NotesLoading(List<Note> notes) : super(notes);

  @override
  List<Object> get props => [notes];
}

class NotesLoaded extends NotesState {
  NotesLoaded(List<Note> notes) : super(notes);

  @override
  List<Object> get props => [notes];
}

class NotesError extends NotesState {
  final String message;
  NotesError(List<Note> notes, this.message) : super(notes);

  @override
  List<Object> get props => [notes, message];
}
