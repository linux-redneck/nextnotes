import 'package:flutter/foundation.dart';
import 'package:hydrated_bloc/hydrated_bloc.dart';

import 'models/base_note.dart';
import 'models/note.dart';
import 'note_repository.dart';
import 'notes_state.dart';

class NotesCubit extends HydratedCubit<NotesState> {
  final NoteRepository repository;

  NotesCubit(this.repository) : super(NotesInitial());

  getNotes() async {
    emit(NotesLoading([]));
    try {
      final List<BaseNote> baseNotes = await repository.getBaseNotes();
      final List<Note> notes = await Future.wait(baseNotes
          .map((baseNote) async => await repository.completeNote(baseNote)));
      notes.sort((a, b) => -a.modified.compareTo(b.modified));
      emit(NotesLoaded(notes));
    } catch (e) {
      debugPrint(e.toString());
      emit(NotesError([], 'Error loading notes'));
    }
  }

  updateNotes() async {
    List<Note> currentNotes = (state as NotesLoaded).notes;
    emit(NotesLoading(currentNotes));
    try {
      List<BaseNote> currentBaseNotes =
          currentNotes.map((note) => note.toBaseNote()).toList();
      // Download note list from server
      final baseNotes = await repository.getBaseNotes();
      // Remove local notes that are not on the server
      currentNotes.removeWhere((note) =>
          (baseNotes
              .indexWhere((baseNote) => baseNote.filename == note.filename)) <
          0);
      // Remove server notes that are identical to local
      baseNotes.removeWhere((baseNote) => currentBaseNotes.contains(baseNote));
      // Remove local notes that are different from server
      currentNotes.removeWhere((note) => (baseNotes
              .indexWhere((baseNote) => baseNote.filename == note.filename) >=
          0));
      final List<Note> newNotes = await Future.wait(baseNotes
          .map((baseNote) async => await repository.completeNote(baseNote)));
      final notes = currentNotes + newNotes;
      notes.sort((a, b) => -a.modified.compareTo(b.modified));
      emit(NotesLoaded(notes));
    } catch (e) {
      debugPrint(e.toString());
      emit(NotesError(currentNotes, 'Error loading notes'));
    }
  }

  addNote(Note note) async {
    emit(NotesLoading(state.notes));
    try {
      final List<Note> notes = state.notes;
      notes.insert(0, await repository.addNote(note));
      emit(NotesLoaded(notes));
    } catch (e) {
      debugPrint(e.toString());
      emit(NotesError(state.notes, 'Error saving note'));
    }
  }

  editNote(Note note) async {
    emit(NotesLoading(state.notes));
    try {
      await repository.editNote(note);
      final List<Note> notes = state.notes.map((e) {
        if (e.filename == note.filename)
          return note;
        else
          return e;
      }).toList();
      emit(NotesLoaded(notes));
    } catch (e) {
      debugPrint(e.toString());
      emit(NotesError(state.notes, 'Error saving note'));
    }
  }

  deleteNote(Note note) async {
    emit(NotesLoading(state.notes));
    try {
      await repository.deleteNote(note);
      final notes = state.notes.where((e) => e != note).toList();
      emit(NotesLoaded(notes));
    } catch (e) {
      debugPrint(e.toString());
      emit(NotesError(state.notes, 'Error deleting note'));
    }
  }

  @override
  NotesState fromJson(Map<String, dynamic> json) {
    List<Note> notes = [];
    json['notes'].forEach((el) => notes.add(Note.fromJson(el)));
    return NotesLoaded(notes);
  }

  @override
  Map<String, dynamic> toJson(NotesState state) {
    if (state is NotesLoaded) {
      var json = {'notes': []};
      state.notes.forEach((note) => json['notes'].add(note.toJson()));
      return json;
    } else
      return null;
  }
}
